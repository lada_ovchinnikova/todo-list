const button = document.querySelector(".button-add");
const task = document.querySelector(".form-control");
const parent = document.querySelector("ul");
const inputParent = document.querySelector(".input-area");
const cancelEditing = document.querySelector(".cancel-edit-button");
 
button.addEventListener("click",  
e => {
  if(!e.target.classList.contains("upload")) {
    const newElement = document.createElement("li");
    newElement.classList.add("list-group-item", "d-flex", "justify-content-between", "align-items-start");
    newElement.innerHTML = `
    <div class="ms-2 me-auto">
       <input type="checkbox" class="check-box">
       <span class="qwe">${task.value}</span>
       </div>
     <span class="badge bg-danger rounded-pill del">x</span>
    `
    task.value = ""
    return parent.appendChild(newElement); 
  }
debugger
  const editValue = document.querySelector(".edit-value");
  editValue.innerHTML = `${task.value}`
  editValue.classList.toggle("edit-value")

  editValue.parentNode.parentNode.classList.toggle("actived")
  task.value = ""
  changeTextButtonAdd();
  hideCancelEditButton();
  }
);
 
showCancelEditButton = () => {
  return cancelEditing.classList.remove("cancel-invisible")
}

hideCancelEditButton = () => {
  return cancelEditing.classList.add("cancel-invisible")
}

cancelEditing.addEventListener("click",  
  e => {
    debugger
    const editValue = document.querySelector(".edit-value");
    editValue.classList.toggle("edit-value")
    const aciveElement = document.querySelector(".actived");
    aciveElement.classList.remove("actived");
    task.value = "";
    changeTextButtonAdd();
    hideCancelEditButton();
  }
);

// зачеркивает задание
parent.addEventListener("click",  
  e => {
    if(!e.target.classList.contains("check-box")) {
    return
  };
  e.target.parentNode.classList.toggle("line-through")
  }
);


//  удаляет элемент
parent.addEventListener("click",  
  e => {
  if(!e.target.classList.contains("del")) {
  return
  };
  parent.removeChild(e.target.parentNode)
  }
);

//  меняет текс основной кнопки
const changeTextButton = () => {
  button.classList.remove("button-add")
  button.classList.add("upload");
  return button.textContent = "Обновить"
}
const changeTextButtonAdd = () => {
  button.classList.toggle("upload");
  button.classList.add("button-add");
  return button.textContent = "Добавить"
}
 
parent.addEventListener("click",  
  e => {
  if(!e.target.classList.contains("qwe")) {
  return
  };
  
  task.value = e.target.textContent;
  e.target.classList.add("edit-value")
  for (const li of document.querySelectorAll('ul li')) {
    li.classList.remove('actived');
  }
  e.target.parentNode.parentNode.classList.add("actived")
  changeTextButton();
  showCancelEditButton();
  }
);